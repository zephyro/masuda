// --- Top Nav

$(function() {
    var pull = $('.nav_toggle');
    var menu = $('.header');

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('open_nav');
    });
});


$(function($){
    var header = $('.header');
    $(window).scroll(function(){

        if ( $(window).scrollTop() > 0) {
            header.addClass('fix_top');
        }else{
            header.removeClass('fix_top');
        }
    });
});